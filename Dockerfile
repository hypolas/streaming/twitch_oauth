FROM golang:bullseye

RUN mkdir /app && apt-get update -y && apt-get upgrade -y
COPY bin/oauth_twitch /app/
CMD ["/app/oauth_twitch"]