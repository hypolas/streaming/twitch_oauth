package main

import (
	oidc "github.com/coreos/go-oidc"
	"github.com/futurenda/google-auth-id-token-verifier"
	"golang.org/x/oauth2"
	"net/http"
)

func twitchVerify(idToken *oidc.IDToken) error {
	if err := idToken.Claims(&claims); err != nil {
		return AnnotateError(
			err,
			"Couldn't verify your confirmation, please try again (twitch).",
			http.StatusBadRequest,
		)
	}
	return nil
}

func youtubeVerify(token *oauth2.Token) error {
	v := googleAuthIDTokenVerifier.Verifier{}
	err := v.VerifyIDToken(token.AccessToken, []string{
		clientID,
	})
	if err == nil {
		_, err := googleAuthIDTokenVerifier.Decode(token.AccessToken)
		if err != nil {
			return AnnotateError(
				err,
				"Couldn't verify your confirmation, please try again (youtube).",
				http.StatusBadRequest,
			)
		}
	}

	return nil
}
