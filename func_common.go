package main

import (
	"encoding/json"
	"fmt"
	"os"
	"strings"
)

func getEnv() error {
	if clientID = os.Getenv("CLIENTID"); clientID == "" {
		return fmt.Errorf("CLIENTID est manquant")
	}
	if clientSecret = os.Getenv("CLIENTSECRET"); clientSecret == "" {
		return fmt.Errorf("CLIENTSECRET est manquant")
	}

	if scopeEnv := os.Getenv("SCOPES"); scopeEnv == "" {
		return fmt.Errorf("SCOPES est manquant")
	} else {
		sco = strings.Split(strings.ReplaceAll(scopeEnv, " ", ""), ",")
		fmt.Printf("SCOPES => \n%v\n", sco)
	}
	if cookieScret = os.Getenv("COOKIESECRET"); cookieScret == "" {
		return fmt.Errorf("COOKIESECRET est manquant")
	}
	if redirectURL = os.Getenv("REDIRECTURL"); redirectURL == "" {
		return fmt.Errorf("REDIRECTURL est manquant")
	}
	if oidcIssuer = os.Getenv("OIDCISSUER"); oidcIssuer == "" {
		return fmt.Errorf("OIDCISSUER est manquant")
	}

	if claim = os.Getenv("CLAIMS"); claim == "" {
		return fmt.Errorf("CLAIMS est manquant")
	}

	return nil
}

type Response struct {
	Token string
}

func printJson(dta interface{}) {
	j, _ := json.MarshalIndent(dta, "", "  ")
	fmt.Println(string(j))
}
