/*
Copyright 2018 Amazon.com, Inc. or its affiliates. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at

	http://aws.amazon.com/apache2.0/

	or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.

*/

package main

import (
	"context"
	"crypto/rand"
	"crypto/tls"
	"encoding/gob"
	"encoding/hex"
	// "encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"strings"

	"github.com/arriqaaq/flashdb"
	oidc "github.com/coreos/go-oidc"
	"github.com/gorilla/sessions"
	"golang.org/x/oauth2"
)

const (
	stateCallbackKey = "oauth-state-callback"
	oauthSessionName = "oauth-session"
	oauthTokenKey    = "oauth-token"
	// serviceOidcIssuer = map[string]string{
	// 	"twitch": "https://id.twitch.tv/oauth2",
	// 	"trovo":  "https://open.trovo.live/page/login.html",
	// }
)

type Db struct {
	Data *flashdb.FlashDB
}

type Verify func(*oauth2.Token) error

var (
	db           Db
	claim        string
	claims       oauth2.AuthCodeOption
	sco          []string
	cookieScret  string
	clientID     string
	clientSecret string
	redirectURL  string
	oidcVerifier *oidc.IDTokenVerifier
	cookieStore  *sessions.CookieStore
	oidcIssuer   string
	scopes       []string
	oauth2Config *oauth2.Config
	ctx          context.Context
	cookieSecret []byte
	// verify       = map[string]Verify{
	// 	"twitch":  twitchVerify,
	// 	"youtube": youtubeVerify,
	// }
)

// HandleRoot is a Handler that shows a login button. In production, if the frontend is served / generated
// by Go, it should use html/template to prevent XSS attacks.
func HandleRoot(w http.ResponseWriter, r *http.Request) (err error) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(`<html><body><a href="/login">Login</a></body></html>`))
	return
}

// HandleLogin is a Handler that redirects the user to Twitch for login, and provides the 'state'
// parameter which protects against login CSRF.
func HandleLogin(w http.ResponseWriter, r *http.Request) (err error) {
	session, err := cookieStore.Get(r, oauthSessionName)
	if err != nil {
		log.Printf("corrupted session %s -- generated new", err)
		err = nil
	}

	tokenBytes := make([]byte, 32)
	if _, err := rand.Read(tokenBytes[:]); err != nil {
		return AnnotateError(err, "Couldn't generate a session!", http.StatusInternalServerError)
	}

	fullState := hex.EncodeToString(tokenBytes[:])
	state := fullState[:32]
	session.Values[stateCallbackKey] = state

	if err = session.Save(r, w); err != nil {
		return
	}

	newHandler := r.FormValue("receive")
	session.Values["receive"] = newHandler
	servicename := r.FormValue("servicename")
	session.Values["servicename"] = servicename
	// session.AddFlash(newHandler, "receive")
	if err = session.Save(r, w); err != nil {
		return
	}

	http.Redirect(w, r, oauth2Config.AuthCodeURL(state, claims), http.StatusTemporaryRedirect)

	return
}

// HandleOauth2Callback is a Handler for oauth's 'redirect_uri' endpoint;
// it validates the state token and retrieves an OAuth token from the request parameters.
func HandleOAuth2Callback(w http.ResponseWriter, r *http.Request) (err error) {
	session, err := cookieStore.Get(r, oauthSessionName)
	if err != nil {
		log.Printf("corrupted session %s -- generated new", err)
		err = nil
	}

	// ensure we flush the csrf challenge even if the request is ultimately unsuccessful
	defer func() {
		if err := session.Save(r, w); err != nil {
			log.Printf("error saving session: %s", err)
		}
	}()

	switch stateChallenge, state := session.Values[stateCallbackKey], r.FormValue("state"); {
	case state == "", session.Values[stateCallbackKey] == "":
		err = errors.New("missing state challenge")
	case state != stateChallenge:
		err = fmt.Errorf("invalid oauth state, expected '%s', got '%s'", state, stateChallenge)
	}

	if err != nil {
		return AnnotateError(
			err,
			"couldn't verify your confirmation, please try again.",
			http.StatusBadRequest,
		)
	}

	token, err := oauth2Config.Exchange(ctx, r.FormValue("code"))
	if err != nil {
		return
	}

	// add the oauth token to session
	// session.Values[oauthTokenKey] = token
	// if err = session.Save(r, w); err != nil {
	// 	return
	// }

	// bytesj, _ := json.MarshalIndent(token, "", "  ")
	// log.Println(string(bytesj))

	newHandler := ""
	for key := range session.Values {
		if key == "receive" {
			newHandler = session.Values["receive"].(string)
		}
	}

	rawIDToken, ok := token.Extra("id_token").(string)
	if !ok {
		return AnnotateError(
			fmt.Errorf("can't extract id token from access token"),
			"couldn't verify your confirmation, please try again.",
			http.StatusBadRequest,
		)
	}

	db.dbUpdate(map[string]map[string]string{newHandler: {
		"access_token": token.AccessToken,
		"id_token":     rawIDToken},
	})

	idToken, err := oidcVerifier.Verify(context.Background(), rawIDToken)
	if err != nil {
		return AnnotateError(
			err,
			"Couldn't verify your confirmation, please try again (twitch).",
			http.StatusBadRequest,
		)
	}

	var claims struct {
		Iss   string `json:"iss"`
		Sub   string `json:"sub"`
		Aud   string `json:"aud"`
		Exp   int32  `json:"exp"`
		Iat   int32  `json:"iat"`
		Nonce string `json:"nonce"`
		Email string `json:"email"`
	}

	if err := idToken.Claims(&claims); err != nil {
		return AnnotateError(
			err,
			"Couldn't verify your confirmation, please try again (twitch).",
			http.StatusBadRequest,
		)
	}

	// verify[session.Values["servicename"].(string)](token)

	http.Redirect(w, r, "/", http.StatusTemporaryRedirect)

	return
}

// HumanReadableError represents error information
// that can be fed back to a human user.
//
// This prevents internal state that might be sensitive
// being leaked to the outside world.
//
// It's also useful because raw error strings rarely make much
// sense to a human.
type HumanReadableError interface {
	HumanError() string
	HTTPCode() int
}

// HumanReadableWrapper implements HumanReadableError
type HumanReadableWrapper struct {
	ToHuman string
	Code    int
	error
}

func (h HumanReadableWrapper) HumanError() string { return h.ToHuman }
func (h HumanReadableWrapper) HTTPCode() int      { return h.Code }

// AnnotateError wraps an error with a message that is intended for a human end-user to read,
// plus an associated HTTP error code.
func AnnotateError(err error, annotation string, code int) error {
	if err == nil {
		return nil
	}
	return HumanReadableWrapper{ToHuman: annotation, error: err}
}

type Handler func(http.ResponseWriter, *http.Request) error

func main() {
	initDataBase()
	defer db.Data.Close()

	// Load Configs
	err := getEnv()
	if err != nil {
		panic(err)
	}
	scopes = sco

	claims = oauth2.SetAuthURLParam("claims", claim)

	cookieSecret = []byte(cookieScret)
	cookieStore = sessions.NewCookieStore(cookieSecret)

	// Create contect
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: tr}
	ctx = oidc.ClientContext(context.Background(), client)

	// Gob encoding for gorilla/sessions
	gob.Register(&oauth2.Token{})

	provider, err := oidc.NewProvider(ctx, oidcIssuer)
	if err != nil {
		log.Fatal(err)
	}

	oauth2Config = &oauth2.Config{
		ClientID:     clientID,
		ClientSecret: clientSecret,
		Scopes:       scopes,
		Endpoint:     provider.Endpoint(),
		RedirectURL:  redirectURL,
	}

	oidcVerifier = provider.Verifier(&oidc.Config{
		ClientID:        clientID,
		SkipIssuerCheck: true,
	})

	var middleware = func(h Handler) Handler {
		return func(w http.ResponseWriter, r *http.Request) (err error) {
			// parse POST body, limit request size
			if err = r.ParseForm(); err != nil {
				return AnnotateError(err, "Something went wrong! Please try again.", http.StatusBadRequest)
			}

			return h(w, r)
		}
	}

	// errorHandling is a middleware that centralises error handling.
	// this prevents a lot of duplication and prevents issues where a missing
	// return causes an error to be printed, but functionality to otherwise continue
	// see https://blog.golang.org/error-handling-and-go
	var errorHandling = func(handler func(w http.ResponseWriter, r *http.Request) error) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if err := handler(w, r); err != nil {
				var errorString string = "Something went wrong! Please try again."
				var errorCode int = 500

				if v, ok := err.(HumanReadableError); ok {
					errorString, errorCode = v.HumanError(), v.HTTPCode()
				}

				log.Println(err)
				w.Write([]byte(errorString))
				w.WriteHeader(errorCode)
				return
			}
		})
	}

	var handleFunc = func(path string, handler Handler) {
		http.Handle(path, errorHandling(middleware(handler)))
	}

	// http.Handle("/", http.FileServer(http.Dir("./static/")))
	handleFunc("/", HandleRoot)
	handleFunc("/login", HandleLogin)
	handleFunc("/redirect", HandleOAuth2Callback)
	handleFunc("/gto", GetToken)

	fmt.Printf("Started running on %s\n", strings.ReplaceAll(redirectURL, "redirect", ""))
	fmt.Println(http.ListenAndServe(":7001", nil))
}

func GetToken(w http.ResponseWriter, r *http.Request) (err error) {
	keys := []string{}
	keys = append(keys, "access_token", "id_token")
	getData := map[string][]string{
		r.FormValue("receive"): keys}

	t := db.dbGet(getData)
	for _, value := range t {
		for subkey := range value {
			w.Header().Set(subkey, value[subkey])
		}
	}

	w.WriteHeader(http.StatusOK)

	db.dbDelete(r.FormValue("receive"))
	return
}
