package main

import (
	"github.com/arriqaaq/flashdb"
	"log"
)

func initDataBase() {
	var err error
	config := &flashdb.Config{Path: "/path", EvictionInterval: 10}
	db.Data, err = flashdb.New(config)
	if err != nil {
		log.Fatal(err)
	}
}

func (d *Db) dbUpdate(data map[string]map[string]string) {
	err := d.Data.Update(func(tx *flashdb.Tx) error {
		for key, valueType := range data {
			for typeVal, value := range valueType {
				log.Println(key, typeVal, value)
				_, err := tx.HSet(key, typeVal, value)
				tx.Expire(key, 120)
				if err != nil {
					return err
				}
			}
		}
		return nil
	})
	if err != nil {
		log.Fatal(err)
	}
}

func (d *Db) dbDelete(key string) {
	var data map[string]string = make(map[string]string)
	data[key] = ""
	err := d.Data.Update(func(tx *flashdb.Tx) error {
		for key := range data {
			err := tx.Set(key, "")
			return err
		}
		return nil
	})
	if err != nil {
		log.Fatal(err)
	}
}

func (d *Db) dbGet(key map[string][]string) map[string]map[string]string {
	returnThis := make(map[string]map[string]string)
	d.Data.View(func(tx *flashdb.Tx) error {
		for key, value := range key {
			if _, ok := returnThis[key]; !ok {
				returnThis[key] = make(map[string]string)
			}
			for _, val := range value {
				returnThis[key][val] = tx.HGet(key, val)
			}
		}
		return nil
	})

	return returnThis
}
