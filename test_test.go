package main

import (
	"log"
	"testing"
)

func TestDB(t *testing.T) {
	initDataBase()
	defer db.Data.Close()

	db.dbUpdate(map[string]map[string]string{"base": {
		"access_token": "toto",
		"id_token":     "tata"},
	})

	keys := []string{}
	keys = append(keys, "access_token", "id_token")
	getData := map[string][]string{
		"base": keys}

	te := db.dbGet(getData)
	printJson(te)

	for _, value := range te {
		for subkey := range value {
			log.Println(subkey, value[subkey])
			// w.Header().Set(key, subkey)
		}
	}
}
