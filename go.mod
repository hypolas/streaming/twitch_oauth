module oauth_twitch

go 1.21.4

require (
	github.com/arriqaaq/flashdb v0.1.6
	github.com/coreos/go-oidc v2.2.1+incompatible
	github.com/futurenda/google-auth-id-token-verifier v0.0.0-20170311140316-2a5b89f28b7e
	github.com/gorilla/sessions v1.2.2
	golang.org/x/oauth2 v0.15.0
)

require (
	github.com/arriqaaq/aol v0.1.2 // indirect
	github.com/arriqaaq/art v0.1.2 // indirect
	github.com/arriqaaq/hash v0.1.2 // indirect
	github.com/arriqaaq/set v0.1.2 // indirect
	github.com/arriqaaq/zset v0.1.2 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/gorilla/securecookie v1.1.2 // indirect
	github.com/pquerna/cachecontrol v0.2.0 // indirect
	golang.org/x/crypto v0.16.0 // indirect
	google.golang.org/appengine v1.6.8 // indirect
	google.golang.org/protobuf v1.31.0 // indirect
	gopkg.in/square/go-jose.v2 v2.6.0 // indirect
)
